Source: python-qinlingclient
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 debhelper-compat (= 10),
 dh-python,
 openstack-pkg-tools,
 python3-all,
 python3-pbr (>= 2.0.0),
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 python3-babel,
 python3-coverage <!nocheck>,
 python3-fixtures <!nocheck>,
 python3-hacking,
 python3-iso8601,
 python3-keystoneclient (>= 3.8.0),
 python3-openstackclient (>= 3.12.0),
 python3-openstackdocstheme (>= 1.18.1) <!nodoc>,
 python3-openstackdocstheme <!nodoc>,
 python3-os-testr (>= 1.0.0),
 python3-osc-lib (>= 1.11.0),
 python3-oslo.i18n (>= 3.15.3),
 python3-oslo.log (>= 3.36.0),
 python3-oslo.serialization (>= 2.18.0),
 python3-oslo.utils (>= 3.33.0),
 python3-oslotest (>= 1:3.2.0) <!nocheck>,
 python3-prettytable,
 python3-reno <!nodoc>,
 python3-requests (>= 2.14.2),
 python3-requests-mock (>= 1.2.0) <!nocheck>,
 python3-six,
 python3-stestr (>= 2.0.0) <!nocheck>,
 python3-testscenarios <!nocheck>,
 python3-testtools (>= 2.2.0) <!nocheck>,
 python3-yaml,
Standards-Version: 4.2.0
Vcs-Browser: https://salsa.debian.org/openstack-team/clients/python-qinlingclient
Vcs-Git: https://salsa.debian.org/openstack-team/clients/python-qinlingclient.git
Homepage: https://docs.openstack.org/qinling/latest/

Package: python-qinlingclient-doc
Build-Profiles: <!nodoc>
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: client for Function as a Service for OpenStack - doc
 Qinling (is pronounced "tʃinliŋ") refers to Qinling Mountains in southern
 Shaanxi Province in China. The mountains provide a natural boundary between
 North and South China and support a huge variety of plant and wildlife, some
 of which is found nowhere else on Earth.
 .
 Qinling is Function as a Service for OpenStack. This project aims to provide a
 platform to support serverless functions (like AWS Lambda). Qinling supports
 different container orchestration platforms (Kubernetes/Swarm, etc.) and
 different function package storage backends (local/Swift/S3) by nature using
 plugin mechanism.
 .
 This package contains the documentation.

Package: python3-qinlingclient
Architecture: all
Depends:
 python3-babel,
 python3-iso8601,
 python3-keystoneclient (>= 3.8.0),
 python3-openstackclient (>= 3.12.0),
 python3-osc-lib (>= 1.11.0),
 python3-oslo.i18n (>= 3.15.3),
 python3-oslo.log (>= 3.36.0),
 python3-oslo.serialization (>= 2.18.0),
 python3-oslo.utils (>= 3.33.0),
 python3-pbr (>= 2.0.0),
 python3-prettytable,
 python3-requests (>= 2.14.2),
 python3-six,
 python3-yaml,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-qinlingclient-doc,
Description: client for Function as a Service for OpenStack - Python 3.x
 Qinling (is pronounced "tʃinliŋ") refers to Qinling Mountains in southern
 Shaanxi Province in China. The mountains provide a natural boundary between
 North and South China and support a huge variety of plant and wildlife, some
 of which is found nowhere else on Earth.
 .
 Qinling is Function as a Service for OpenStack. This project aims to provide a
 platform to support serverless functions (like AWS Lambda). Qinling supports
 different container orchestration platforms (Kubernetes/Swarm, etc.) and
 different function package storage backends (local/Swift/S3) by nature using
 plugin mechanism.
 .
 This package contains the Python 3.x module.
